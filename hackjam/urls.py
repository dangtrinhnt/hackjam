from django.conf.urls import patterns, include, url
from hackjam.apps.main.forms import HackJamAuthenticationForm
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'hackjam.apps.main.views.home', name='home'),
    # url(r'^hackjam/', include('hackjam.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^olympus/', include(admin.site.urls)),
    url(r'^login/?$','django.contrib.auth.views.login',{'template_name':'login.html', 'authentication_form':HackJamAuthenticationForm}),
    url(r'^logout/$', 'hackjam.apps.main.views.log_out', name='logout'),
)
