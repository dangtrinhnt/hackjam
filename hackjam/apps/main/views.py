from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from hackjam.apps.main import models as main_models




@login_required
def home(request):
	return render_to_response('home.html', locals(),\
						context_instance=RequestContext(request))


def log_out(request):
	logout(request)
	return redirect('home')